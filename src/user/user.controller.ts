import {BadRequestException, Controller, Get, HttpCode} from '@nestjs/common';
import {UserService} from "./user.service";
import {ApiTags, ApiOperation} from "@nestjs/swagger";

@Controller('user')
@ApiTags('Users')
export class UserController {
    constructor(private usersService: UserService) {}
    @Get()
    @HttpCode(201)
    @ApiOperation({
        summary: "Please open in a separate browser tab",
        description: "This is only for testing purposes, please open it in a separate tab, for easier copy paste"
    })
    async createKey(): Promise<{ privateKey: string, publicKey: string }> {
        try{
            return this.usersService.createKey()
        } catch (e) {
            throw new BadRequestException(e.message)
        }
    }
    @Get()
    @ApiOperation({
        summary: "fetches private and public keys",
        description: "this is to ease the testing process only, in real life example, this would be saved in Fort Knox"
    })
    async fetchKey(): Promise<string> {
        try{
            return this.usersService.fetchKey()
        } catch (e) {
            throw new BadRequestException(e.message)
        }
    }


}
