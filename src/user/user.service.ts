import {Injectable, Logger} from '@nestjs/common';
import {generateKeyPair} from "crypto";
import {UserDocument} from "./user.schema";
import {Model} from "mongoose";
import {InjectModel} from "@nestjs/mongoose";

@Injectable()
export class UserService {
    private privateKey;
    private publicKey;
    constructor(
        private logger: Logger,
        @InjectModel('User') private userModel: Model<UserDocument>
    ) {
        this.userModel.findOne().exec().then(user => {
            if(!user){
                this.createKey().then(({privateKey, publicKey}) => {
                    this.privateKey = privateKey
                    this.publicKey = publicKey
                    const user = new this.userModel({
                        name:'default',
                        privateKey: this.privateKey,
                        publicKey: this.publicKey,
                        passphrase: ''
                    })
                    user.save()
                })
            }
        })

    }

    async createKey(): Promise<{ privateKey: string, publicKey: string }>  {
        return new Promise((resolve, reject) => {
            generateKeyPair('rsa', {
                modulusLength: 4096,
                publicKeyEncoding: {
                    type: 'pkcs1',
                    format: 'pem'
                },
                privateKeyEncoding: {
                    type: 'pkcs8',
                    format: 'pem',
                    cipher: 'aes-256-cbc',
                    passphrase: ''
                }
            },async(err, publicKey, privateKey) => {
                if (err) return reject(err);

                // for testing purposes only
                this.logger.log('\n'+publicKey);
                this.logger.log('\n'+privateKey);
                // for testing purposes only

                this.privateKey = privateKey;
                this.publicKey = publicKey;
                resolve({publicKey, privateKey});
            });
        });
    }

    async fetchKey(): Promise<string> {
        const user = await this.userModel.findOne().exec();
        return user.privateKey
    }
}
