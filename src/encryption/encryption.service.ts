import { Injectable } from '@nestjs/common';
import {publicEncrypt, privateDecrypt} from "crypto";

@Injectable()
export class EncryptionService {
    async decrypt(encrypted, key, passphrase = ''): Promise<string> {
        try {
            const decrypted = await privateDecrypt(
                {key, passphrase},
                Buffer.from(encrypted, "base64")
            );
            return decrypted.toString("utf8");
        }catch (e) {
            throw new Error(e.message)
        }
    }

    async encrypt(text, publicKey): Promise<string> {
        try {
            const encrypted = await publicEncrypt(publicKey, new Buffer(text))
            return encrypted.toString("base64");
        }catch (e) {
            throw new Error(e.message)
        }
    }
}
