import {Controller, Get, Redirect} from '@nestjs/common';
import {ApiTags} from "@nestjs/swagger";

@Controller()
@ApiTags(null)
export class AppController {
  @Get()
  @Redirect('/api')
  redirectToSwagger(): void {}
}
