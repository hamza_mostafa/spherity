import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { UserModule } from './user/user.module';
import { SecretNoteModule } from './secret-note/secret-note.module';
import {ConfigModule, ConfigService} from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import {EncryptionService} from "./encryption/encryption.service";

@Module({
  imports: [
    UserModule,
    SecretNoteModule,
    ConfigModule.forRoot({
    isGlobal: true,
  }),
    MongooseModule.forRoot((new ConfigService).get('DB_URL'))
  ],
  controllers: [AppController],
  providers: [EncryptionService],
})
export class AppModule {}
