import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

@Schema()
export class SecretNote {
    @Prop()
    name: string;
    @Prop()
    note: string;
}

export type SecretNoteDocument = SecretNote & Document;

export const SecretNoteSchema = SchemaFactory.createForClass(SecretNote);
