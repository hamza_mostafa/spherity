import {Logger, Module} from '@nestjs/common';
import { SecretNoteController } from './secret-note.controller';
import { SecretNoteService } from './secret-note.service';
import {EncryptionService} from "../encryption/encryption.service";
import {MongooseModule} from "@nestjs/mongoose";
import {UserSchema} from "../user/user.schema";
import {SecretNoteSchema} from "./secret-note.schema";

@Module({
  imports: [MongooseModule.forFeature([
    { name: 'User', schema: UserSchema },
    { name: 'SecretNote', schema: SecretNoteSchema },
  ])],
  controllers: [SecretNoteController],
  providers: [Logger, SecretNoteService, EncryptionService]
})
export class SecretNoteModule {}
