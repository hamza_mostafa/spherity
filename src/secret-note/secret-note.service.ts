import {Injectable, NotFoundException} from '@nestjs/common';
import {SecretNote, SecretNoteDocument} from "./secret-note.schema";
import {Model} from "mongoose";
import {UserDocument} from "../user/user.schema";
import {EncryptionService} from "../encryption/encryption.service";
import {InjectModel} from "@nestjs/mongoose";


@Injectable()
export class SecretNoteService {
    constructor(
        @InjectModel('SecretNote') private secretNoteModel: Model<SecretNoteDocument>,
        @InjectModel('User') private userModel: Model<UserDocument>,
        private encryptionService: EncryptionService) {
    }

    async create(text): Promise<string> {
            // fetched from auth
            const user = await this.userModel.findOne().exec();
            const encrypted = await this.encryptionService.encrypt(text, user.publicKey);
            const secretNote = new this.secretNoteModel({
                name: 'Actual text: '+ text,
                note: encrypted
            })
            secretNote.save()
            return secretNote._id
    }

    async update(id: string, text: string): Promise<string> {
        // fetched from auth
        const user = await this.userModel.findOne().exec();
        // check if ownership through user_id field `not implemented`
        // check the decryption with privateKey `not implemented`

        const encrypted = await this.encryptionService.encrypt(text, user.publicKey);
        const secretNote = await this.secretNoteModel.findByIdAndUpdate(id, {
            name: 'Actual text: '+ text,
            note: encrypted
        });
        return secretNote.id
    }

    async fetchEncrypted(id: string): Promise<string>{
        const secretNote = await this.secretNoteModel.findById(id).exec();
        return secretNote.note;
    }

    async fetchDecrypted(id: string): Promise<string>{
        const secretNote = await this.secretNoteModel.findById(id).exec();
        // fetched from auth
        const user = await this.userModel.findOne().exec();
        return this.encryptionService.decrypt(secretNote.note, user.privateKey);
    }

    async destroy(id: string): Promise<void>{
       const res = await this.secretNoteModel.findByIdAndDelete(id).exec();
       if(!res){
           throw new NotFoundException('has been deleted already or wrong ID')
       }
    }
}
