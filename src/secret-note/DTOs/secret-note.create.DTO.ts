import { IsNotEmpty } from 'class-validator';

export class CreateSecretNoteDTO {
    @IsNotEmpty()
    note: string;
}
