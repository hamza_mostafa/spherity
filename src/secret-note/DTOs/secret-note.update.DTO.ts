import { PartialType } from '@nestjs/mapped-types';
import {CreateSecretNoteDTO} from "./secret-note.create.DTO";
import { IsNotEmpty } from 'class-validator';

export class UpdateSecretNoteDTO extends PartialType(CreateSecretNoteDTO){
    @IsNotEmpty()
    id: string;
}
