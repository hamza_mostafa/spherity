import {BadRequestException, Body, Controller, Delete, Get, HttpCode, Logger, Post, Put, Req} from '@nestjs/common';
import {CreateSecretNoteDTO} from "./DTOs/secret-note.create.DTO";
import {ApiBody, ApiOperation, ApiTags} from "@nestjs/swagger";
import {UpdateSecretNoteDTO} from "./DTOs/secret-note.update.DTO";
import {SecretNoteService} from "./secret-note.service";


@Controller('secret-note')
@ApiTags('SecretNotes')
export class SecretNoteController {
    constructor(private logger: Logger, private secretNoteService: SecretNoteService) {}

    @Post('/')
    @HttpCode(201)
    @ApiBody({
        type: CreateSecretNoteDTO,
        description: 'creates secret note',
        examples: {
            1: {
                summary: "Test Note",
                description: "testing the note creation",
                value: {note: "test note"} as CreateSecretNoteDTO
            }
        }
    })
    create(@Body() secretNoteDTO: CreateSecretNoteDTO): Promise<string> {
        try {
            this.logger.log('a new key is created for user x under IP ........')
            return this.secretNoteService.create(secretNoteDTO.note);
        }catch (e) {
            throw new BadRequestException(e.message)
        }
    }

    @Get('/encrypted/:id')
    @ApiOperation({
        parameters: [{name: 'id', in: 'path'}]
    })
    fetchEncrypted(@Req() req): Promise<string> {
        try {
            return this.secretNoteService.fetchEncrypted(req.params.id);
        }catch (e) {
            throw new BadRequestException(e.message)
        }
    }
    @Get('/decrypted/:id')
    @ApiOperation({
        parameters: [{name: 'id', in: 'path'}]
    })
    fetch(@Req() req): Promise<String> {
        try {
            return this.secretNoteService.fetchDecrypted(req.params.id)
        }catch (e) {
            throw new BadRequestException(e.message)
        }
    }

    @Put('/:id')
    @ApiOperation({
        parameters: [{name: 'id', in: 'path'}]
    })
    @ApiBody({
        type: UpdateSecretNoteDTO,
        description: 'updates secret note',
        examples: {
            1: {
                summary: "Test Note",
                description: "Please update the id to a correct one",
                value: {note: "test note", id: '62e885ef52595fc7e7e032ac'} as UpdateSecretNoteDTO
            }
        }
    })
    update(@Body() updateSecretNoteDTO: UpdateSecretNoteDTO): Promise<string> {
        try {
            return this.secretNoteService.update(updateSecretNoteDTO.id, updateSecretNoteDTO.note);
        }catch (e) {
            throw new BadRequestException(e.message)
        }
    }

    @Delete('/:id')
    @ApiOperation({
        parameters: [{name: 'id', in: 'path'}]
    })
    @HttpCode(204)
    destroy(@Req() req): Promise<void> {
        try {
            return this.secretNoteService.destroy(req.params.id);
        }catch (e) {
            throw new BadRequestException(e.message)
        }
    }

}
