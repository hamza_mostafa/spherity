FROM node:18

# Create app directory
WORKDIR /app

COPY . /app

# ONLY IN DEVELOPMENT
COPY .env.example /app/.env

# Install app dependencies
RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

EXPOSE 3000
CMD [ "npm", "run","start:dev" ]
# If you are building your code for production
#CMD ["npm", "run", "build" ]
#CMD ["npm", "run", "start:prod" ]
